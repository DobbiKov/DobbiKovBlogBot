# DobbiKov Telegram Bot

Developer: [Roman Zapotockiy](https://t.me/dobbi_crmp) **(dobbikov)**

## Stack
- С#
- Console App .NET Core
- Telegram.BOT nuget package

## Setup
1. Install [.NET Core SDK](https://dotnet.microsoft.com/download)
2. Install nuget package [Telegram.BOT](https://www.nuget.org/packages/Telegram.Bot/)
3. Start project